# Iso Library

[![Gitter](https://badges.gitter.im/officialexosoft/Iso-Engine.svg)](https://gitter.im/officialexosoft/Iso-Engine?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=5TC7HWQ2AGGQW&lc=US&item_name=Exoplanet%20Software&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHosted)

The Iso Library is the first effort by Exosoft's lead developer [GingerDeadshot](https://github.com/gingerdeadshot) to make a game library for two-dimensional orthographic platformers, such as [Zerfall](https://github.com/officialexosoft/zerfall-java). The original code for Iso came from Zerfall, and was branched off into its own separate project.

This library utilizes projects by other developers. These include:
- [TinySound](https://github.com/finnkuuisto/TinySound) by GitHub user [finnkuuisto](https://github.com/finnkuuisto)
