package exosoft.util;

import javafx.scene.image.Image;

import java.io.*;

/**
 * Created as an implementation of javafx image, with serialization.
 */
public class SerialImage implements Serializable {
  private static final long serialVersionUID = -1L;
  private transient Image image;
  private File file;

  public SerialImage(String path) {
    image = new Image(path);
    file = new File(path);
  }

  public SerialImage(File file) {
    this(file.getPath());
  }

  private void writeObject(ObjectOutputStream stream) throws IOException {
    stream.writeObject(file);
  }

  private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
    file = (File) stream.readObject();
    image = new Image(file.getPath());
  }

  public Image getImage() {
    return image;
  }

  public String getPath() {
    return file.getPath();
  }


}
