package exosoft.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by campbell on 8/10/16.
 */
public class FileUtil {
  /**
   * Converts a URL object to a File object using the toURI method.
   * If the syntax of the URL is invalid for a URI object, the file is created using the
   * getPath method.
   *
   * @param url The URL object to be converted.
   * @return Returns a File representation of the URL object.
   */
  public static File toFile(URL url) {
    File file;
    try {
      file = new File(url.toURI());
    } catch (URISyntaxException e) {
      file = new File(url.getPath());
    }
    return file;
  }

  /**
   * Returns a URL using the ClassLoader. Relative paths will start at the classpath directory.
   *
   * @param path The resource path.
   * @return A URL representation of the resource.
   */
  public static URL getResource(String path) {
    ClassLoader cl = FileUtil.class.getClassLoader();
    return cl.getResource(path);
  }

  /**
   * Returns an InputStream using the ClassLoader. Relative paths will start at the classpath
   * directory.
   *
   * @param path The resource path.
   * @return An InputStream representation of the resource.
   */
  public static InputStream getResourceStream(String path) {
    ClassLoader cl = FileUtil.class.getClassLoader();
    return cl.getResourceAsStream(path);
  }

  /**
   * Returns a URL from the path used to load classes. Relative paths will start
   * at the class loader directory.
   *
   * @param path The resource path.
   * @return A URL representation of the resource.
   */
  public static URL getSystemResource(String path) {
    return ClassLoader.getSystemResource(path);
  }
  
  /**
   * Returns an InputStream from the path used to load classes. Relative paths will start
   * at the class loader directory.
   *
   * @param path The resource path.
   * @return An InputStream representation of the resource.
   */
  public static InputStream getSystemResourceStream(String path) {
    return ClassLoader.getSystemResourceAsStream(path);
  }

  /**
  * Reads the contents of a text file to a String.
  * 
  * @param path The file path.
  * @param encoding The file encoding.
  * @return The contents of the file.
  */
  static String readFile(String path, Charset encoding) throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }
}
