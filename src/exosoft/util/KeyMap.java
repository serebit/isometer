package exosoft.util;

import javafx.scene.input.KeyCode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by campbell on 7/17/16.
 */
public class KeyMap {
    private static Map<KeyCode, KeyState> keys = new HashMap<>();

    public static KeyState getKeyState(KeyCode key) {
        if (!keys.containsKey(key)) {
            keys.put(key, KeyState.IDLE);
        }
        KeyState keyState = keys.get(key);
        if (keyState == KeyState.RELEASE) {
            keys.put(key, KeyState.IDLE);
            return KeyState.RELEASE;
        }
        else if (keyState == KeyState.PRESS) {
            keys.put(key, KeyState.ACTIVE);
            return KeyState.PRESS;
        }
        return keyState;
    }

    public static void pressKey(KeyCode key) {
        if (!getKeyState(key).equals(KeyState.ACTIVE)) {
            keys.put(key, KeyState.PRESS);
        }
    }

    public static void releaseKey(KeyCode key) {
        keys.put(key, KeyState.RELEASE);
    }
}
