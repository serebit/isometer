package exosoft.util;

/**
 * Created by campbell on 7/13/16.
 */
public enum KeyState {
    PRESS, ACTIVE, RELEASE, IDLE
}
