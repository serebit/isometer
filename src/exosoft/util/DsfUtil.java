package exosoft.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import javafx.fxml.FXMLLoader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * A parser for data serialization formats such as XML and JSON.
 */
public class DsfUtil {

  public static Document parseXml(String path) throws IOException {
    return parseXml(new File(path));
  }

  private static Document parseXml(File file) throws IOException {
    Document doc = null;
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      doc = db.parse(file);
      doc.normalize();
    } catch (ParserConfigurationException | SAXException e) {
      System.err.print("Error while parsing file at path ".concat(file.getPath()));
      e.printStackTrace();
    }
    return doc;
  }

  public static <T> T parseFXML(String path) {
    try {
      return FXMLLoader.load(FileUtil.getResource(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static JsonNode parseJson(String path) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readTree(FileUtil.getResource(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static JsonNode parseJson(InputStream in) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readTree(in);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static boolean validateJson(String jsonPath, String schemaPath) throws IOException, ProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode json = mapper.readTree(new File(jsonPath));
    JsonNode schemaNode = mapper.readTree(new File(schemaPath));
    JsonNode schemaIdentifier = schemaNode.get("$schema");
    if (null == schemaIdentifier) {
      ((ObjectNode) schemaNode).put("$schema", "http://json-schema.org/draft-04/schema#");
    }
    JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    JsonSchema schema = factory.getJsonSchema(schemaNode);
    ProcessingReport report = schema.validate(json);
    if (!report.isSuccess()) {
      for (ProcessingMessage processingMessage : report) {
        throw new ProcessingException(processingMessage);
      }
    }
    return report.isSuccess();
  }
}