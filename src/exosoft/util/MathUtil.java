package exosoft.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by campbell on 7/18/16.
 */
public class MathUtil {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static double average(double... values) {
        double sum = 0;
        for (double val : values) {
            sum += val;
        }
        return sum / values.length;
    }

    public static BigDecimal round(BigDecimal value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        value = value.setScale(places, RoundingMode.HALF_UP);
        return value;
    }

    public static BigDecimal roundAndConvert(double value, int places) {
        return BigDecimal.valueOf(round(value, places));
    }

    public static BigDecimal average(BigDecimal... values) {
        double sum = 0;
        for (BigDecimal val : values) {
            sum += val.doubleValue();
        }
        return new BigDecimal(sum / values.length);
    }

    public static boolean lessThan(BigDecimal val1, BigDecimal val2) {
        int compareValue = val1.compareTo(val2);
        return (compareValue < 0);
    }

    public static boolean greaterThan(BigDecimal val1, BigDecimal val2) {
        int compareValue = val1.compareTo(val2);
        return (compareValue > 0);
    }
}
