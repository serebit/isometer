package exosoft.iso.world;

import exosoft.util.SerialImage;

import javax.annotation.Nullable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * An item that can be interacted with.
 */
public class InteractiveItem extends Item {
  private ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(1);
  private Runnable action;
  private Future<?> task;
  private String dialog;

  public InteractiveItem() {
    super();
    this.setAction(null);
    this.task = null;
    this.dialog = null;
  }

  public InteractiveItem(Runnable interact,
                         @Nullable String dialog,
                         SerialImage texture,
                         Layer distance,
                         double x, double y, double w, double h) {
    super();
    this.setAction(interact);
    this.setDialog(dialog);
    this.setTexture(texture);
    this.setDistance(distance);
    this.setGeometry(x, y, w, h);
  }

  public InteractiveItem(Runnable interact,
                         @Nullable String dialog,
                         Layer distance,
                         double x,
                         double y,
                         double w,
                         double h) {
    super(distance, x, y, w, h);
    this.setAction(interact);
    this.setDialog(dialog);
  }

  public InteractiveItem(@Nullable String dialog, Layer distance,
                         double x, double y, double w, double h) {
    super(distance, x, y, w, h);
    this.setDialog(dialog);
  }

  public InteractiveItem(@Nullable String dialog, SerialImage texture, Layer distance,
                         double x, double y, double w, double h) {
    super(distance, x, y, w, h);
    this.setTexture(texture);
    this.setDialog(dialog);
  }

  public InteractiveItem(Layer distance,
                         double x, double y, double w, double h) {
    super(distance, x, y, w, h);
    this.setDialog(dialog);
  }

  public void setAction(Runnable interact) {
    this.action = interact;
  }

  public void runAction() {
    if (task == null || task.isDone()) {
      task = scheduler.submit(action);
    }
  }

  public String getDialog() {
    return dialog;
  }

  public void setDialog(@Nullable String dialog) {
    this.dialog = dialog;
  }
}
