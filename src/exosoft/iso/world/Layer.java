package exosoft.iso.world;

import javafx.scene.paint.Color;

/**
 *
 */
public enum Layer {
  LAYER2, LAYER3, LAYER4, LAYER5, LAYER1;
  private Color defaultStroke;
  private Color defaultFill;
  public int layer;

  static {
    exosoft.iso.world.Layer.LAYER1.defaultFill = Color.rgb(250, 250, 250);
    exosoft.iso.world.Layer.LAYER1.defaultStroke = Color.rgb(245, 245, 245);
    exosoft.iso.world.Layer.LAYER1.layer = 1;
    exosoft.iso.world.Layer.LAYER2.defaultFill = Color.rgb(238, 238, 238);
    exosoft.iso.world.Layer.LAYER2.defaultStroke = Color.rgb(224, 224, 224);
    exosoft.iso.world.Layer.LAYER2.layer = 2;
    exosoft.iso.world.Layer.LAYER3.defaultFill = Color.rgb(189, 189, 189);
    exosoft.iso.world.Layer.LAYER3.defaultStroke = Color.rgb(158, 158, 158);
    exosoft.iso.world.Layer.LAYER3.layer = 3;
    exosoft.iso.world.Layer.LAYER4.defaultFill = Color.rgb(117, 117, 117);
    exosoft.iso.world.Layer.LAYER4.defaultStroke = Color.rgb(97, 97, 97);
    exosoft.iso.world.Layer.LAYER4.layer = 4;
    exosoft.iso.world.Layer.LAYER5.defaultFill = Color.rgb(66, 66, 66);
    exosoft.iso.world.Layer.LAYER5.defaultStroke = Color.rgb(33, 33, 33);
    exosoft.iso.world.Layer.LAYER5.layer = 5;
  }

  public Color getDefaultFill() {
    return defaultFill;
  }

  public Color getDefaultStroke() {
    return defaultStroke;
  }
}
