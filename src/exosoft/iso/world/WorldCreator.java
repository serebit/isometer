package exosoft.iso.world;

import java.io.*;

/**
 * Creates a world and serializes it as such for later use.
 */
class WorldCreator {
  public static void generateWorld(Environment environment, String filename) {
    ObjectOutputStream oos;
    OutputStream fout;
    File saveFile = new File(filename);
    try {
      fout = new FileOutputStream(saveFile, false);
      oos = new ObjectOutputStream(fout);
      oos.writeObject(environment);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
