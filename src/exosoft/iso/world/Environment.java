package exosoft.iso.world;

import exosoft.util.KeyMap;
import exosoft.util.KeyState;
import exosoft.util.SerialImage;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The game world. Contains items (such as floors, platforms and walls) and
 * nonPlayerCharacters (such as the player).
 * <p>
 * Due to the nature of JavaFX, the GraphicsContext is not stored in the save
 * file. The main class must pass the currently loaded Canvas to the loaded Environment.
 */
public abstract class Environment implements Serializable {
  private static final long serialVersionUID = -6717458311378967244L;

  private List<CollectableItem> collectableItems;
  private List<Item> dynamicItems = new ArrayList<>();
  private List<Item> staticItems;
  private List<InteractiveItem> interactiveItems;

  private List<Npc> nonPlayerCharacters = new ArrayList<>();
  private Player player;

  private SerialImage skybox;
  private double gravity;
  private transient Map<Integer, GraphicsContext> layers = new HashMap<>();
  private transient GraphicsContext skyboxLayer;
  private transient Pane headsUpDisplay;

  private List<Runnable> scripts;


  public Environment(StackPane container, Pane headsUpDisplay, double gravity) {
    setCanvas(container);
    setHud(headsUpDisplay);
    this.gravity = gravity;
    player = null;
    skybox = null;
  }

  public void spawnNpc(Npc npc, double x, double y) {
    nonPlayerCharacters.add(npc);
    npc.setGravity(gravity);
    npc.setLocation(x, y);
  }

  public void addItem(InteractiveItem item) {
    if (interactiveItems == null) {
      interactiveItems = new ArrayList<>();
    }
    interactiveItems.add(item);
  }

  public void addItem(CollectableItem item) {
    if (collectableItems == null) {
      collectableItems = new ArrayList<>();
    }
    collectableItems.add(item);
  }

  public void addItem(Item item) {
    if (staticItems == null) {
      staticItems = new ArrayList<>();
    }
    staticItems.add(item);
  }

  public void removeItem(InteractiveItem item) {
    interactiveItems.remove(item);
  }

  public void createItem(Layer distance, double x, double y, double w, double h) {
    staticItems.add(new Item(distance, x, y, w, h));
  }

  public void setSkybox(SerialImage skybox) {
    this.skybox = skybox;
  }

  public synchronized void execute() {
    {
      player.storeLocation();
      player.getInput();
      player.movement();
      player.physics();
      player.mechanics();
      if (player.isShooting()) {
        Point2D bulletOrigin = player.getBulletOrigin();
        double bulletAngle = player.getBulletAngle();
        double bulletX = bulletOrigin.getX();
        double bulletY = bulletOrigin.getY();
        double scanDistance = 8000;
        double scanWidth = scanDistance * Math.cos(Math.toRadians(bulletAngle));
        double scanHeight = scanDistance * Math.sin(Math.toRadians(bulletAngle));
        Line bulletTrajectory = new Line(bulletX, bulletY, scanWidth, scanHeight);
        Item hitItem = null;
        Entity hitEntity = null;
        List<Item> items = intersectItems(Layer.LAYER3, bulletTrajectory);
        List<Entity> entities = intersectNpcs(bulletTrajectory);
        
        scan:
        for (
            double scanX = bulletX,
            scanY = bulletY;
            Math.abs(scanX) < Math.abs(bulletX) + scanWidth ||
            Math.abs(scanY) < Math.abs(bulletY) + scanHeight;
            scanX += scanWidth / scanDistance,
            scanY += scanHeight / scanDistance
        ) {
          Point2D scan = new Point2D(scanX, bulletY);
          for (Item item : items) {
            if (item.contains(scan)) {
              hitItem = item;
              break scancheck;
            }
          }
          for (Entity entity : entities) {
            if (entity.getBounds().contains(scan)) {
              hitEntity = entity;
              break scancheck;
            }
          }
        }
        if (hitEntity != null) {
          hitEntity.damage(player.getGun().getDamage());
        }
      }
      player.finish();
    }

    List<Npc> npcsToRemove = new ArrayList<>();
    for (Npc npc : nonPlayerCharacters) {
      npc.storeLocation();
      npc.movement();
      npc.physics();
      npc.mechanics();
      if (npc.getHealth() <= 0) {
        npcsToRemove.add(npc);
      }
    }
    nonPlayerCharacters.removeAll(npcsToRemove);

    for (Item item : staticItems) {
      Rectangle itemBounds = item.getBounds();
      if (item.getLayer().equals(Layer.LAYER3)) {
        for (Entity entity : nonPlayerCharacters) {
          Rectangle entityBounds = entity.getBounds();
          Bounds intersect = Shape.intersect(entityBounds, itemBounds).getBoundsInLocal();
          if (intersect.isEmpty()) continue;
          entity.collide(item.getBounds());
        }
        {
          Rectangle playerBounds = player.getBounds();
          Bounds intersect = Shape.intersect(playerBounds, itemBounds).getBoundsInLocal();
          if (intersect.isEmpty()) continue;
          player.collide(item.getBounds());
        }
      }
      if (scripts != null) {
        scripts.forEach(Runnable::run);
      }
    }

    KeyState fKey = KeyMap.getKeyState(KeyCode.F);

    for (InteractiveItem item : interactiveItems) {
      Rectangle itemBounds = item.getBounds();
      itemBounds.setX(itemBounds.getX() - 10);
      itemBounds.setY(itemBounds.getY() - 10);
      itemBounds.setWidth(itemBounds.getWidth() + 20);
      itemBounds.setHeight(itemBounds.getHeight() + 20);
      {
        Rectangle playerBounds = player.getBounds();
        Bounds intersect = Shape.intersect(playerBounds, itemBounds).getBoundsInLocal();
        Text interactDialog = (Text) headsUpDisplay.lookup("#interact-dialog");
        interactDialog.setVisible(false);
        if (intersect.isEmpty()) {
          itemBounds.setX(itemBounds.getX() + 10);
          itemBounds.setY(itemBounds.getY() + 10);
          itemBounds.setWidth(itemBounds.getWidth() - 20);
          itemBounds.setHeight(itemBounds.getHeight() - 20);
          continue;
        }
        interactDialog.setVisible(true);
        interactDialog.setText(item.getDialog());
        if (fKey.equals(KeyState.RELEASE)) {
          item.runAction();
        }
        itemBounds.setX(itemBounds.getX() + 10);
        itemBounds.setY(itemBounds.getY() + 10);
        itemBounds.setWidth(itemBounds.getWidth() - 20);
        itemBounds.setHeight(itemBounds.getHeight() - 20);
        break;
      }
    }

    for (InteractiveItem item : interactiveItems) {
      if (item.getLayer().equals(Layer.LAYER3)) {
        Rectangle itemBounds = item.getBounds();
        for (Entity entity : nonPlayerCharacters) {
          Rectangle entityBounds = entity.getBounds();
          Bounds intersect = Shape.intersect(entityBounds, itemBounds).getBoundsInLocal();
          if (intersect.isEmpty()) continue;
          entity.collide(item.getBounds());
        }
        {
          Rectangle playerBounds = player.getBounds();
          Bounds intersect = Shape.intersect(playerBounds, itemBounds).getBoundsInLocal();
          if (intersect.isEmpty()) continue;
          player.collide(item.getBounds());
        }
      }
    }

    for (CollectableItem item : collectableItems) {
      Rectangle itemBounds = item.getBounds();
      {
        Rectangle playerBounds = player.getBounds();
        Bounds intersect = Shape.intersect(playerBounds, itemBounds).getBoundsInLocal();
        Text interactDialog = (Text) headsUpDisplay.lookup("#interact-dialog");
        interactDialog.setVisible(false);
        if (intersect.isEmpty()) continue;
        interactDialog.setVisible(true);
        interactDialog.setText(item.getDialog());
        if (fKey.equals(KeyState.RELEASE)) {
          item.collect();
          if (item.collected) {
            collectableItems.remove(item);
          }
        }
        break;
      }
    }
  }

  public void visual() {
    nonPlayerCharacters.forEach(Entity::visual);
    player.visual();
  }

  private List<Item> compileItems(Layer layer) {
    List<Item> list = new ArrayList<>();
    list.addAll(staticItems.stream().filter(item -> item.getLayer().equals
        (layer)).collect(Collectors.toList()));
    list.addAll(dynamicItems.stream().filter(item -> item.getLayer().equals
        (layer)).collect(Collectors.toList()));
    list.addAll(interactiveItems.stream().filter(item -> item.getLayer().equals
        (layer)).collect(Collectors.toList()));
    return list;
  }

  private List<Item> intersectItems(Layer layer, Line line) {
    List<Item> layerItems = compileItems(layer);
    return layerItems.stream().filter(item ->
        !Shape.intersect(line, item).getBoundsInLocal().isEmpty()).collect(Collectors.toList());
  }

  private List<Entity> intersectNpcs(Line line) {

    return nonPlayerCharacters.stream().filter(npc -> !Shape.intersect(line, npc.getBounds())
        .getBoundsInLocal().isEmpty()).collect(Collectors.toList());
  }

  public synchronized void drawWorld() {
    double displayWidth = layers.get(5).getCanvas().getWidth();
    double displayHeight = layers.get(5).getCanvas().getHeight();
    double xTranslate = -player.getX() + (displayWidth / 2) - player.getBounds().getWidth() / 2;
    double yTranslate = -player.getY() + (displayHeight / 2) - player.getBounds().getHeight() / 2;
    double skyboxDist = 0.35;

    skyboxLayer.scale(skyboxDist, skyboxDist);
    skyboxLayer.drawImage(skybox.getImage(), 0, 0);
    skyboxLayer.scale(1 / skyboxDist, 1 / skyboxDist);

    for (int layer : layers.keySet()) {
      layers.get(layer).clearRect(0, 0, displayWidth, displayHeight);
      layers.get(layer).translate(xTranslate, yTranslate);
    }
    staticItems.forEach(item -> item.draw(layers.get(item.getLayer().layer)));
    interactiveItems.forEach(item -> item.draw(layers.get(item.getLayer().layer)));
    layers.get(3).drawImage(player.drawSprite(), player.getX(), player.getY());
    for (Entity entity : nonPlayerCharacters) {
      layers.get(3).drawImage(entity.drawSprite(), entity.getX(), entity.getY());
    }
    for (int layer : layers.keySet()) {
      layers.get(layer).translate(-xTranslate, -yTranslate);
    }
  }

  private void setCanvas(StackPane container) {
    ObservableList<Node> layerCanvases = container.getChildren();
    for (int i = 5; i > 0; i--) {
      Canvas layer = new Canvas(container.getWidth(), container.getHeight());
      layer.widthProperty().bind(container.widthProperty());
      layer.heightProperty().bind(container.heightProperty());
      layerCanvases.add(layer);
      layers.put(i, layer.getGraphicsContext2D());
    }
    Canvas layer = new Canvas(container.getWidth(), container.getHeight());
    layer.widthProperty().bind(container.widthProperty());
    layer.heightProperty().bind(container.heightProperty());
    skyboxLayer = layer.getGraphicsContext2D();
  }

  public void spawnPlayer(Player p, int x, int y) {
    player = p;
    player.setGravity(gravity);
    player.setLocation(x, y);
  }

  public Player getPlayer() {
    return player;
  }

  public void addScript(Runnable runnable) {
    if (scripts == null) {
      scripts = new ArrayList<>();
    }
    scripts.add(runnable);
  }

  private void setHud(Pane headsUpDisplay) {
    this.headsUpDisplay = headsUpDisplay;
  }
}
