package exosoft.iso.world;

import javax.annotation.Nullable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * An item that can be interacted with.
 */
public class CollectableItem extends Item {
  private Runnable action;
  private Future<?> task;
  private ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(1);
  private String dialog;
  public boolean collected;

  public CollectableItem(Runnable interact, @Nullable String dialog, Layer distance,
                         double x, double y, double w, double h) {
    super(distance, x, y, w, h);
    this.setAction(interact);
    this.setDialog(dialog);
  }

  public void setAction(Runnable interact) {
    this.action = interact;
  }

  public void collect() {
    if (task == null || task.isDone()) {
      task = scheduler.submit(action);
    }
    scheduler.shutdown();
    try {
      scheduler.awaitTermination(10, TimeUnit.MINUTES);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public String getDialog() {
    return dialog;
  }

  private void setDialog(@Nullable String dialog) {
    this.dialog = dialog;
  }
}
