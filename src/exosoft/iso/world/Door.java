package exosoft.iso.world;

import com.sun.istack.internal.NotNull;
import kuusisto.tinysound.Sound;

import javax.annotation.Nullable;

/**
 * It's a door.
 */
public class Door extends InteractiveItem {
  private Sound openSound;
  private boolean isOpen;

  public Door(@Nullable String dialog, @NotNull Sound open, double x, double y, double w, double
      h) {
    super();
    this.setDistance(Layer.LAYER3);
    this.setDialog(dialog);
    this.openSound = open;
    this.setGeometry(x, y, w, h);
    isOpen = false;
  }

  @Override
  public synchronized void runAction() {
    if (!isOpen) {
      System.out.println("Opening ".concat(toString()));
      setWidth(this.getWidth() + 100);
      setDistance(Layer.LAYER4);
      isOpen = true;
      setDialog(null);
      openSound.play();
    }
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Door [x = ");
    stringBuilder.append(getX());
    stringBuilder.append(", y = ");
    stringBuilder.append(getY());
    stringBuilder.append(", width = ");
    stringBuilder.append(getWidth());
    stringBuilder.append(", height = ");
    stringBuilder.append(getHeight());
    stringBuilder.append(", isOpen = ");
    stringBuilder.append(isOpen);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
