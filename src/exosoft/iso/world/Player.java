package exosoft.iso.world;

import exosoft.iso.world.weapon.Gun;
import javafx.geometry.Point2D;

/**
 *
 */
public abstract class Player extends Entity {
  public abstract void getInput();

  public abstract Gun getGun();

  public abstract void setGun(Gun gun);

  public abstract boolean isShooting();

  public abstract Point2D getBulletOrigin();

  public abstract double getBulletAngle();
}
