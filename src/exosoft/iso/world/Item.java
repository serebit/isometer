package exosoft.iso.world;

import exosoft.util.SerialImage;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;

import java.io.Serializable;

public class Item extends Rectangle implements Serializable {
  private static final long serialVersionUID = -5999543761501930894L;
  private SerialImage texture;
  private Layer distance;

  public Item() {
    this.setTexture(null);
    this.setDistance(null);
  }

  /**
   * Creates a new item. If the number of given points is odd, the last coordinate
   * will be omitted.
   */
  public Item(Layer distance, double x, double y, double w, double h) {
    super(x, y, w, h);
    this.setDistance(distance);
  }

  public Item(SerialImage texture, Layer distance, double x, double y, double w, double h) {
    this(distance, x, y, w, h);
    this.setTexture(texture);
  }

  public Layer getLayer() {
    return distance;
  }

  public void setDistance(Layer distance) {
    this.distance = distance;
  }

  public void setTexture(SerialImage texture) {
    this.texture = texture;
  }

  public void setGeometry(double x, double y, double w, double h) {
    this.setX(x);
    this.setY(y);
    this.setWidth(w);
    this.setHeight(h);
  }

  public GraphicsContext draw(GraphicsContext g) {
    g.setLineWidth(4.0);
    g.setLineCap(StrokeLineCap.ROUND);
    if (texture == null) {
      g.setFill(distance.getDefaultFill());
      g.setStroke(distance.getDefaultStroke());
      g.strokeRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
      g.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    } else {
      g.getCanvas().setClip(this);
      g.drawImage(texture.getImage(), this.getX(), this.getY());
      g.getCanvas().setClip(null);
    }
    return g;
  }

  public Rectangle getBounds() {
    return this;
  }
}
