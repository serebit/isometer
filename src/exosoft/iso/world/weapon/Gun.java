package exosoft.iso.world.weapon;

import com.fasterxml.jackson.databind.JsonNode;
import exosoft.util.DsfUtil;
import exosoft.util.FileUtil;
import kuusisto.tinysound.Music;
import kuusisto.tinysound.Sound;
import kuusisto.tinysound.TinySound;

import java.io.Serializable;
import java.net.URL;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 *
 */
public abstract class Gun implements Serializable {
  final transient ScheduledThreadPoolExecutor scheduler = new ScheduledThreadPoolExecutor(1);
  static final Sound dry;
  Sound fire;
  Music reload;
  int clip;
  int cache;
  int CLIP_SIZE;
  int RELOAD_TIME;
  int DAMAGE;
  double WEIGHT;
  private int CACHE_SIZE;
  private boolean isFiring;
  private String label;

  static {
    URL dryFile = FileUtil.getResource("guns/dry_fire.au");
    dry = TinySound.loadSound(dryFile);
  }

  public Gun(String folder) {
    JsonNode data = DsfUtil.parseJson(folder.concat("meta.json"));
    this.CLIP_SIZE = data.get("clip").asInt();
    this.clip = CLIP_SIZE;

    this.CACHE_SIZE = data.get("cache").asInt();
    this.cache = CACHE_SIZE;

    this.RELOAD_TIME = data.get("reload").asInt();
    this.DAMAGE = data.get("damage").asInt();
    this.WEIGHT = data.get("weight").asDouble();

    this.fire = TinySound.loadSound(FileUtil.getResource(folder.concat("gunshot.au")));
    this.reload = TinySound.loadMusic(FileUtil.getResource(folder.concat("reload.au")));
    this.label = data.get("name").asText();
  }

  void fire() {
    isFiring = true;
  }

  public abstract void close();

  public abstract void pull();

  public abstract void release();

  public abstract void reload();

  public boolean isFiring() {
    return isFiring;
  }

  public void cycle() {
    isFiring = false;
  }

  boolean fullClip() {
    return clip == CLIP_SIZE;
  }

  boolean fullCache() {
    return cache == CACHE_SIZE;
  }

  boolean emptyClip() {
    return clip <= 0;
  }

  boolean emptyCache() {
    return cache <= 0;
  }

  public int getClip() {
    return clip;
  }

  public int getCache() {
    return cache;
  }

  public int getDamage() { return DAMAGE; }
  
  public int 

  public String getName() {
    return label;
  }
}
