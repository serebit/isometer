package exosoft.iso.world.weapon;

import com.fasterxml.jackson.databind.JsonNode;
import exosoft.util.DsfUtil;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Fully automatic abstract weapon. Includes custom fire and reload logic.
 */
public abstract class AutoGun extends Gun {
  private final int RPM;
  private ScheduledFuture<?> cycle;

  protected AutoGun(String folder) {
    super(folder);
    JsonNode data = DsfUtil.parseJson(folder.concat("meta.json"));
    this.RPM = data.get("rate").asInt();
  }

  public final void close() {
    release();
  }

  public final synchronized void pull() {
    if (!reload.playing()) {
      if (!emptyClip()) {
        cycle = scheduler.scheduleAtFixedRate(() -> {
          if (emptyClip()) {
            cycle.cancel(true);
          } else {
            fire();
            fire.play();
            clip--;
          }
        }, (long) 6e5 / RPM, (long) 6e10 / RPM, TimeUnit.NANOSECONDS);
      } else dry.play();
    }
  }

  public final void release() {
    if (cycle != null) {
      cycle.cancel(false);
    }
  }

  public void reload() {
    if (!reload.playing()) {
      if (!fullClip() && !emptyCache()) {
        if (!cycle.isCancelled()) {
          cycle.cancel(true);
        }
        reload.play(false);
        scheduler.schedule(() -> {
          cache -= (CLIP_SIZE - clip);
          clip = CLIP_SIZE;
          reload.stop();
        }, RELOAD_TIME, TimeUnit.MILLISECONDS);
      }
    }
  }
}
