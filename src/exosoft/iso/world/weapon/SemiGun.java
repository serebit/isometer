package exosoft.iso.world.weapon;

import java.util.concurrent.TimeUnit;

/**
 * Semi-automatic abstract weapon. Includes custom fire and reload logic.
 */
public abstract class SemiGun extends Gun {

  protected SemiGun(String folder) {
    super(folder);
  }

  public final void close() {
    //TODO figure out if necessary
  }

  public final void pull() {
    if (!reload.playing()) {
      if (!emptyClip()) {
        fire();
        fire.play();
        clip--;
      } else dry.play();
    }
  }

  public final void release() {
    // TODO figure out if necessary
  }

  public final void reload() {
    if (!reload.playing()) {
      if (!fullClip()) {
        reload.play(false);
        scheduler.schedule(() -> {
          cache -= (CLIP_SIZE - clip);
          clip = CLIP_SIZE;
          reload.stop();
        }, RELOAD_TIME, TimeUnit.MILLISECONDS);
      }
    }
  }
}
