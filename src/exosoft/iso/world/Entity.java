package exosoft.iso.world;

import javafx.scene.image.WritableImage;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Abstract class based off of the Sprite class. For in-game entities that have
 * movement capability and collect with physics. The main class must have its
 * own implementation of this subclass.
 */
public abstract class Entity {

  /**
   * Compensates for collision against shapes.
   *
   * @param object The object to collide with
   */
  public abstract void collide(Shape object);

  public abstract void setLocation(double x, double y);

  public abstract void storeLocation();

  public abstract double getX();

  public abstract double getY();

  public abstract Rectangle getBounds();

  public abstract void movement();

  public abstract void mechanics();

  /**
   * Physics logic. Controls velocity.
   */
  public abstract void physics();

  /**
   * Visual logic. Controls animations.
   */
  public abstract void visual();

  public abstract void finish();

  public abstract void damage(double damage);

  public abstract int getHealth();

  public abstract void setGravity(double g);

  public abstract WritableImage drawSprite();
}