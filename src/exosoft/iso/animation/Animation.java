package exosoft.iso.animation;

import javafx.scene.image.WritableImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by campbell on 6/29/16.
 */
public class Animation {
  private List<WritableImage> frames;
  private int index;
  private String action;
  private String state;
  private String direction;
  private int hash;

  private Animation() {
    this.frames = new ArrayList<>();
    index = 0;
  }

  private Animation(List<WritableImage> frames) {
    this();
    this.frames = frames;
    index = 0;
  }

  Animation(String action, String state, String direction, int hash, List<WritableImage> frames) {
    this(frames);
    this.action = action;
    this.state = state;
    this.direction = direction;
    this.hash = hash;
  }

  Map<String, Object> getAttributes() {
    Map<String, Object> attributes = new HashMap<>();
    attributes.put("action", action);
    attributes.put("state", state);
    attributes.put("direction", direction);
    attributes.put("hash", hash);
    return attributes;
  }

  public String getAction() {
    return action;
  }

  public String getState() {
    return state;
  }

  public String getDirection() {
    return direction;
  }
  
  int getHash() {
    return hash;
  }

  public WritableImage getFrame() {
    WritableImage frame = frames.get(index);
    nextFrame();
    return frame;
  }

  private void nextFrame() {
    index++;
    if (index == frames.size()) {
      reset();
    }
  }

  private void reset() {
    index = 0;
  }
}