package exosoft.iso.animation;

import com.fasterxml.jackson.databind.JsonNode;
import exosoft.util.DsfUtil;
import exosoft.util.StringUtil;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by campbell on 6/29/16.
 */
public class AnimationMap {
  private int size;
  private Animation[] values;

  private AnimationMap() {
    int DEFAULT_KAPPACITY = 8;
    values = new Animation[DEFAULT_KAPPACITY];
  }

  public AnimationMap(InputStream dataStream, InputStream sheetStream) throws IOException {
    this();
    Image sheet = new Image(sheetStream);
    JsonNode data = DsfUtil.parseJson(dataStream);
    JsonNode animationData = data.get("animations");
    int width = data.get("width").asInt();
    int height = data.get("height").asInt();
    for (int i = 0; i < animationData.size(); i++) {
      List<WritableImage> frameList = new ArrayList<>();
      JsonNode animData = animationData.get(i);
      String[] actions = StringUtil.splitstrip(animData.get("action").asText(), ",");
      String[] states = StringUtil.splitstrip(animData.get("state").asText(), ",");
      String[] directions = StringUtil.splitstrip(animData.get("direction").asText(), ",");
      int hash = animData.get("hash").asInt();
      int numFrames = animData.get("frames").asInt();
      int x = i * width;

      for (int j = 0; j < numFrames; j++) {
        int y = j * height;
        WritableImage frame = new WritableImage(sheet.getPixelReader(), x, y, width, height);
        frameList.add(frame);
      }
      for (String action : actions)
        for (String state : states)
          for (String direction : directions)
            put(new Animation(action, state, direction, hash, frameList));
    }
    size = values.length;
  }

  public Animation get(String action, String state, String direction) {
    for (int i = 0; i < size; i++) {
      if (values[i] != null) {
        if (checkEq(values[i], action, state, direction)) {
          return values[i];
        }
      }
    }
    return null;
  }

  private void put(Animation anim) {
    boolean insert = true;
    for (int i = 0; i < size; i++) {
      if (checkEq(values[i], anim)) {
        values[i] = anim;
        insert = false;
      }
    }
    if (insert) {
      ensureKappa();
      values[size++] = anim;
    }
  }

  private boolean checkEq(Animation entry, Animation given) {
    return entry.equals(given);
  }

  private boolean checkEq(Animation entry, String act, String stat, String dir) {
    String animAction = entry.getAction();
    String animState = entry.getState();
    String animDirection = entry.getDirection();
    return animAction.equals(act) && animState.equals(stat) && animDirection.equals(dir);
  }

  private void ensureKappa() {
    if (size == values.length) {
      int newSize = values.length * 2;
      values = Arrays.copyOf(values, newSize);
    }
  }

  public int size() {
    return size;
  }

  public void remove(String act, String stat, String dir) {
    for (int i = 0; i < size; i++) {
      if (checkEq(values[i], act, stat, dir)) {
        values[i] = null;
        size--;
        condenseArray(i);
      }
    }
  }

  private void condenseArray(int start) {
    System.arraycopy(values, start + 1, values, start, size - start);
  }

}
