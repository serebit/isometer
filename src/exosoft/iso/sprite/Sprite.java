package exosoft.iso.sprite;

import com.fasterxml.jackson.databind.JsonNode;
import exosoft.iso.animation.Animation;
import exosoft.iso.animation.AnimationMap;
import exosoft.util.DsfUtil;
import exosoft.util.FileUtil;
import javafx.scene.image.WritableImage;

import java.io.*;

public class Sprite implements Serializable {
  private static final long serialVersionUID = 6186934977840419796L;
  private int width = 0;
  private int height = 0;
  private transient AnimationMap animations;
  private transient Animation activeAnim;
  private transient String dataPath;
  private transient String sheetPath;

  public Sprite(String folder) throws IOException {
    dataPath = folder.concat("/metadata.json");
    sheetPath = folder.concat("/sheet.png");

    InputStream dataStream = FileUtil.getResourceStream(dataPath);
    InputStream sheetStream = FileUtil.getResourceStream(sheetPath);

    JsonNode data = DsfUtil.parseJson(FileUtil.getResourceStream(dataPath));
    width = data.get("width").asInt();
    height = data.get("height").asInt();

    animations = new AnimationMap(dataStream, sheetStream);
  }

  private void writeObject(ObjectOutputStream stream) throws IOException {
    stream.writeObject(dataPath);
    stream.writeObject(sheetPath);
  }

  private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
    dataPath = (String) stream.readObject();
    sheetPath = (String) stream.readObject();

    InputStream dataStream = FileUtil.getResourceStream(dataPath);
    InputStream sheetStream = FileUtil.getResourceStream(sheetPath);

    JsonNode data = DsfUtil.parseJson(FileUtil.getResourceStream(dataPath));

    width = data.get("width").asInt();
    height = data.get("height").asInt();
    animations = new AnimationMap(dataStream, sheetStream);
    playAnim("idle", "normal", "left");
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public void playAnim(String action, String state, String direction) {
    activeAnim = animations.get(action, state, direction);
  }

  public void changeAction(String action) {
    activeAnim = animations.get(action, activeAnim.getState(), activeAnim.getDirection());
  }

  public void changeState(String state) {
    activeAnim = animations.get(activeAnim.getAction(), state, activeAnim.getDirection());
  }

  public void changeDirection(String direction) {
    activeAnim = animations.get(activeAnim.getAction(), activeAnim.getState(), direction);
  }

  public String getAction() {
    return activeAnim.getAction();
  }

  public String getState() {
    return activeAnim.getState();
  }

  public String getDirection() {
    return activeAnim.getDirection();
  }

  public WritableImage get() {
    return activeAnim.getFrame();
  }

  public AnimationMap getAnimations() {
    return animations;
  }
}